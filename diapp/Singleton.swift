//
//  Singleton.swift
//  diapp
//
//  Created by Jin Hong on 2019-03-19.
//  Copyright © 2019 junk. All rights reserved.
//


protocol Singleton {
    func requestModel(_ completion: @escaping(Model?) -> Void)
}

class _Singleton: Singleton {
    func requestModel(_ completion: @escaping(Model?) -> Void) {
        completion(nil)
    }
}
