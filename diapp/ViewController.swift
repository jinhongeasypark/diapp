//
//  ViewController.swift
//  diapp
//
//  Created by Jin Hong on 2019-03-04.
//  Copyright © 2019 junk. All rights reserved.
//

import UIKit

class DependencyResolvingViewController: UIViewController {
    
    // Should not be touched except in tests
    private var resolverSource: ResolverSource? = UIApplication.shared.delegate as? AppDelegate
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        resolveDependenciesFromApplicationContainer()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        resolveDependenciesFromApplicationContainer()
    }
    
    private func resolveDependenciesFromApplicationContainer() {
        resolverSource.flatMap { resolveDependencies(from: $0.resolver) }
    }
    
    func resolveDependencies(from resolver: Resolver) {
        
    }
}

class ViewController: DependencyResolvingViewController {
    private var controller: Controller!
    
    override func resolveDependencies(from resolver: Resolver) {
        controller = resolver.resolve() as Controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        controller.requestModel { model in }
    }
}

