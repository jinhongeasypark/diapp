//
//  HttpClient.swift
//  diapp
//
//  Created by Jin Hong on 2019-03-08.
//  Copyright © 2019 junk. All rights reserved.
//

import Alamofire

private extension HttpMethod {
    var alamofireMethod: HTTPMethod {
        switch self {
        case .get: return .get
        case .post: return .post
        }
    }
}

protocol HttpClient {
    func makeRequest<T: Decodable>(_ endpoint: Endpoint, _ completion: @escaping(T?) -> Void)
}

class _HttpClient: HttpClient {
    private let decoder: JSONDecoder
    private let environment: Environment
    init(decoder: JSONDecoder, environment: Environment) {
        self.decoder = decoder
        self.environment = environment
    }
    
    func makeRequest<T: Decodable>(_ endpoint: Endpoint, _ completion: @escaping(T?) -> Void) {
        let url = environment.baseUrl.appendingPathComponent(endpoint.path)
        let method = endpoint.method.alamofireMethod
        let dataRequest = Alamofire.request(url, method: method)
        dataRequest.responseData { [weak self] response in
            let decodedResponse: T?
            if let _self = self, let data = response.data {
                decodedResponse = try? _self.decoder.decode(T.self, from: data)
            } else {
                decodedResponse = nil
            }
            completion(decodedResponse)
        }
        dataRequest.resume()
    }
}
