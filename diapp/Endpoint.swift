//
//  Endpoint.swift
//  diapp
//
//  Created by Jin Hong on 2019-03-19.
//  Copyright © 2019 junk. All rights reserved.
//

enum HttpMethod {
    case get, post
}

protocol Endpoint {
    var path: String { get }
    var method: HttpMethod { get }
}
