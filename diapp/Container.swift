//
//  Container.swift
//  diapp
//
//  Created by Jin Hong on 2019-03-08.
//  Copyright © 2019 junk. All rights reserved.
//

import Swinject

class Entry<T> {
    private let entry: ServiceEntry<T>
    private var _isSingleton: Bool
    init(registered entry: ServiceEntry<T>) {
        self.entry = entry
        self._isSingleton = false
    }
    var isSingleton: Bool {
        get { return _isSingleton }
        set { _isSingleton = newValue; entry.inObjectScope(newValue ? .container : .graph) }
    }
}

protocol Registrar {
    @discardableResult func register<Service>(_ Protocol: Service.Type, `init`: @escaping (()) -> Service) -> Entry<Service>
    @discardableResult func register<Service, A>(_ Protocol: Service.Type, `init`: @escaping ((A)) -> Service) -> Entry<Service>
    @discardableResult func register<Service, A, B>(_ Protocol: Service.Type, `init`: @escaping ((A, B)) -> Service) -> Entry<Service>
}

protocol Resolver {
    func resolve<T>() -> T
}

class Container: Registrar, Resolver {
    private let container: Swinject.Container
    init(container: Swinject.Container? = nil) {
        self.container = container ?? .init()
    }
    
    @discardableResult
    func register<Service>(_ Protocol: Service.Type, `init`: @escaping (()) -> Service) -> Entry<Service> {
        let entry = container.autoregister(Protocol) { [`init`] arguments -> Service in
            return `init`(arguments)
        }
        return Entry(registered: entry)
    }
    
    @discardableResult
    func register<Service, A>(_ Protocol: Service.Type, `init`: @escaping ((A)) -> Service) -> Entry<Service> {
        let entry = container.autoregister(Protocol) { [`init`] arguments -> Service in
            return `init`(arguments)
        }
        return Entry(registered: entry)
    }
    
    @discardableResult
    func register<Service, A, B>(_ Protocol: Service.Type, `init`: @escaping ((A, B)) -> Service) -> Entry<Service> {
        let entry = container.autoregister(Protocol) { [`init`] arguments -> Service in
            return `init`(arguments)
        }
        return Entry(registered: entry)
    }
    
    func resolve<T>() -> T {
        guard let t = container.resolve(T.self) else { fatalError() }
        return t
    }
}
