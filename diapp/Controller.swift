//
//  Controller.swift
//  diapp
//
//  Created by Jin Hong on 2019-03-19.
//  Copyright © 2019 junk. All rights reserved.
//

protocol Controller {
    func requestModel(_ completion: @escaping (Model?) -> Void)
}

class _Controller: Controller {
    private let client: HttpClient
    init(client: HttpClient) {
        self.client = client
    }
    
    func requestModel(_ completion: @escaping (Model?) -> Void) {
        client.makeRequest(ModelRequest.getModel, completion)
    }
}
