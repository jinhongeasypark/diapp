//
//  Environment.swift
//  diapp
//
//  Created by Jin Hong on 2019-03-19.
//  Copyright © 2019 junk. All rights reserved.
//

import Foundation

protocol Environment {
    var baseUrl: URL { get }
}

class _StagingEnvironment: Environment {
    var baseUrl: URL { return URL(string: "http://localhost:3000")! }
}

class _ProductionEnvironment: Environment {
    var baseUrl: URL { return URL(string: "http://www.google.com")! }
}
