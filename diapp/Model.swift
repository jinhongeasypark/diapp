//
//  Model.swift
//  diapp
//
//  Created by Jin Hong on 2019-03-19.
//  Copyright © 2019 junk. All rights reserved.
//

class Model: Codable {
    let id: String
    let name: String
}

enum ModelRequest: Endpoint {
    case getModel
    case getModels
    var path: String {
        switch self {
        case .getModel: return "/path/to/get/model"
        case .getModels: return "/path/to/multiple/models"
        }
    }
    var method: HttpMethod {
        switch self {
        case .getModel: return .get
        case .getModels: return .get
        }
    }
}
