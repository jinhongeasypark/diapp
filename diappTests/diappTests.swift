//
//  diappTests.swift
//  diappTests
//
//  Created by Jin Hong on 2019-03-04.
//  Copyright © 2019 junk. All rights reserved.
//

import XCTest
@testable import diapp

class MockController: Controller {
    private let expectation: XCTestExpectation
    init(expectation: XCTestExpectation) {
        self.expectation = expectation
    }
    
    func requestModel(_ completion: @escaping (Model?) -> Void) {
        expectation.fulfill()
    }
}

class diappTests: XCTestCase {
    
    var testContainer: Container!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        testContainer = .init()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testViewControllerViewDidLoadCallsControllerRequestModel() {
        let exp = expectation(description: "MockController fulfills on requestModel")
        testContainer.register(Controller.self) { return MockController(expectation: exp) }
        
        let viewController = ViewController()
        viewController.resolveDependencies(from: testContainer)
        viewController.viewDidLoad()
        waitForExpectations(timeout: 1)
    }

}
